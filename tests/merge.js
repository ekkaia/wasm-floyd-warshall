function mergeSort(T) {
	if (T.length <= 1) {
		return T;
	} else {
		const A = T.slice(0, T.length / 2);
		const B = T.slice(T.length / 2, T.length);
		return merge(mergeSort(A), mergeSort(B));
	}
}

function merge(A, B) {
	if (A.length === 0) {
		return B;
	} else if (B.length === 0) {
		return A;
	} else if (A[0] <= B[0]) {
		return A.slice(0, 1).concat(merge(A.slice(1, A.length), B));
	} else {
		return B.slice(0, 1).concat(merge(A, B.slice(1, B.length)));
	}
}
const arr = Array.from({ length: 1000 }, () => Math.floor(Math.random() * 1000));

console.time("merge");
const result = mergeSort(arr);
console.timeEnd("merge");

// console.log(result);
