const myModule = require("..");
const {
	mergeSort,
	Uint32Array_ID,
	__newArray,
	__getArrayView,
	__pin,
	__unpin,
} = myModule;

/*
const fooPtr = __pin(getFoo()); // pin if necessary
const foo = Foo.wrap(fooPtr);
const strPtr = foo.getString();
console.log(__getString(strPtr));
__unpin(fooPtr); // unpin if necessary

function doFloydWashall(values) {
	const arrPtr = __newArray(Uint32Array_ID, values);
	const fwPtr = __pin(floydWarshall(arrPtr, 2));
	const view = __getArrayView(arrPtr);
	return { fwPtr, view };
}

const doFw = doFloydWashall([0, 5, 0, 7]);
console.log(doFw.view);
__unpin(doFw.fwPtr);
*/
/*
function mergeSort(T) {
	if (T.length <= 1) {
		return T;
	} else {
		const A = T.slice(0, T.length / 2);
		const B = T.slice(T.length / 2, T.length);
		return merge(mergeSort(A), mergeSort(B));
	}
}

function merge(A, B) {
	if (A.length === 0) {
		return B;
	} else if (B.length === 0) {
		return A;
	} else if (A[0] <= B[0]) {
		return A.slice(0, 1).concat(merge(A.slice(1, A.length), B));
	} else {
		return B.slice(0, 1).concat(merge(A, B.slice(1, B.length)));
	}
}
*/

function doMergeSort(arr) {
	const arrPtr = __newArray(Uint32Array_ID, arr);
	console.time("mergeWasm");
	const arrPtrResult = __pin(mergeSort(arrPtr));
	console.timeEnd("mergeWasm");
	const view = __getArrayView(arrPtrResult);
	return { arrPtrResult, view };
}

const arr = Array.from({ length: 1000 }, () => Math.floor(Math.random() * 1000));
const sortedArr = doMergeSort(arr);
__unpin(sortedArr.ptr);
