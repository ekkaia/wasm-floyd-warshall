export function mergeSort(T: Array<u32>): Array<u32> {
	if (T.length <= 1) {
		return T;
	} else {
		const A = T.slice(0, T.length / 2);
		const B = T.slice(T.length / 2, T.length);
		return merge(mergeSort(A), mergeSort(B));
	}
}

export function merge(A: Array<u32>, B: Array<u32>): Array<u32> {
	if (A.length === 0) {
		return B;
	} else if (B.length === 0) {
		return A;
	} else if (A[0] <= B[0]) {
		return A.slice(0, 1).concat(merge(A.slice(1, A.length), B));
	} else {
		return B.slice(0, 1).concat(merge(A, B.slice(1, B.length)));
	}
}

export const Uint32Array_ID = idof<Array<u32>>()