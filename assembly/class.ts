export class Foo {
	constructor(public str: string) {}
	getString(): string {
		return this.str;
	}
}

export function getFoo(): Foo {
	return new Foo("Hello world!");
}