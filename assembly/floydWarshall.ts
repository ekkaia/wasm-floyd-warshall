export function floydWarshall(matrix: Uint32Array, width: i32): Uint32Array {
	const w = matrix;
	for (let i = 0; i < width; i++) {
		for (let j = 0; j < width; j++) {
			for (let k = 0; k < width; k++) {
				w[j * width + k] = min(
					w[j * width + k],
					w[j * width + i] + w[i * width + k]
				);
			}
		}
	}
	return w;
}

export const Uint32Array_ID = idof<Uint32Array>();
